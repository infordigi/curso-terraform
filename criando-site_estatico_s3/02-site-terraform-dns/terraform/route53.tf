# resource "aws_route53_zone" "primary" {
#   name = "${var.domain}"
# }

data "aws_route53_zone" "site" {
  name         = "${var.domain}"
  private_zone = true
}

resource "aws_route53_record" "site" {
  zone_id = "${data.aws_route53_zone.site.zone_id}"
}
