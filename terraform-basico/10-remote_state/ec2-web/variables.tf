variable "region" {
	default = "us-east-1"
}

variable "ami" {
	default = "ami-0a313d6098716f372"
}

variable "instance_type" {
	default = "t2.micro"
}

variable "vpc_sg_ids" {
  default = "sg-083e2699d400aa088"
}

variable "subnet" {
  default = "subnet-07ded6e074ae64674"
}


