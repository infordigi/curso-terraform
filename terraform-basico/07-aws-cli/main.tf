# Configure the AWS Provider
provider "aws" {
  region = "${var.region}"
}

# Create a instance
resource "aws_s3_bucket" "b" {
  bucket = "my-tf-bucket-terraformsigmast"
  acl    = "private"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}
