# Configure the AWS Provider
provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}

# Create a instance
resource "aws_instance" "web" {
  ami = "${var.ami}"
  instance_type = "${var.type}"

  # ipv6_addresses = "${var.ips}"

  tags = "${var.tags}"
	vpc_security_group_ids = ["sg-083e2699d400aa088"]
	subnet_id = "subnet-07ded6e074ae64674"
}

